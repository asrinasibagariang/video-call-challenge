import html2canvas from "html2canvas";

const CaptureImage = () => {
  const snap = () => {
    html2canvas(document.body).then(function (canvas) {
      var a = document.createElement("a");
      a.href = canvas.toDataURL("..assets/image/jpg").replace("image/jpg", "image/octet-system");
      a.download = "capture-image.jpg";
      a.click();
    });
  };
  return <button onClick={snap}>Take Screenshoot</button>;
};

export default CaptureImage;
