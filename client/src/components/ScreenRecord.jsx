import * as React from "react";

import useScreenRecorder from "use-screen-recorder";

const ScreenRecorder = () => {
  const { blobUrl, pauseRecording, resetRecording, resumeRecording, startRecording, status, stopRecording } = useScreenRecorder({ audio: true });

  return (
    <div>
      {/* <video src={blobUrl} /> */}
      <div className="col">
        <div className="row">
          <small>Status: {status}</small>
        </div>
        <div className="row">
          <button onClick={startRecording}>Start Recording</button>
          <button onClick={stopRecording}>Stop Recording</button>
          <button onClick={pauseRecording}>Pause Recording</button>
          <button onClick={resumeRecording}>Resume Recording</button>
          <button onClick={resetRecording}>Reset Recording</button>
        </div>
        <div className="row">
          <a href={blobUrl} download>
            Click to download
          </a>
        </div>
      </div>
    </div>
  );
};

export default ScreenRecorder;
